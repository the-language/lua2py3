/*
    The Language
    Copyright (C) 2019  Zaoqi <zaomir@outlook.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

function print_loc_of(x) {
  const {
    loc: {
      start: {
        line: loc_start_line,
        column: loc_start_column
      },
      end: {
        line: loc_end_line,
        column: loc_end_column
      }
    }
  } = x
  return `((start (line ${loc_start_line}) (column ${loc_start_column})) (end (line ${loc_end_line}) (column ${loc_end_column})))`
}

function py_indent(code: Array<string>): Array<string> {
  return code.map((x) => '    ' + x)
}

type LuaAST = any

const lua_parse: (x: string) => LuaAST = (() => {
  const p = require('luaparse')

  function luaparse(x) {
    const ast = p.parse(x, {
      comments: false,
      scope: false,
      locations: true
    })
    return ast
  }
  return luaparse
})()

export { lua_parse }

// like https://docs.racket-lang.org/reference/parameters.html
type Parameter<T> = (x?: T) => T
function make_parameter<T>(v: T): Parameter<T> {
  let context: T = v
  const nothing: T = ([] as unknown) as T

  function parameter(arg: T = nothing): T {
    if (arg !== nothing) {
      context = arg
    }
    return context
  }
  return parameter
}

function parameterize<T, R>(v: Parameter<T>, x: T, body: () => R): R {
  const before = v()
  v(x)
  const ret = body()
  v(before)
  return ret
}

const assert_strict: any = require('assert').strict

function assert_equal<T>(x: T, y: T, msg: () => string | null = () => null) { // msg懶惰求值。
  assert_strict.equal(x, y, {
    toString() {
      return msg();
    }
  })
}

function assert_notEqual<T>(x: T, y: T, msg: () => (string | null) = () => null) {
  assert_strict.notEqual(x, y, {
    toString() {
      return msg();
    }
  })
}

function assert_fail(msg: string | null = null): never {
  return assert_strict.ok(false, msg) as never
}

function assert_true(x: boolean, msg: () => (string | null) = () => null) {
  assert_strict.ok(x, {
    toString() {
      return msg();
    }
  })
}

function assert_false(x: boolean, msg: () => (string | null) = () => null) {
  return assert_true(!x, msg)
}

type LuaID = string
type PyID = string
class Scope {
  is_global: boolean

  local: Record<LuaID, PyID>
  not_local__py_local: Record<LuaID, PyID>
  nonlocal: Record<LuaID, PyID>
  global: Record<LuaID, PyID>

  is_py_global: boolean

  py_local: Record<PyID, true>
  py_nonlocal: Record<PyID, true>
  py_global: Record<PyID, true>

  used_py_nonlocal: Record<PyID, true>
  used_py_global: Record<PyID, true>

  _gensym_c: number
  gensym(): string {
    this._gensym_c++
    return "tmp____" + this._gensym_c.toString()
  }
  constructor(args: {
    is_global: boolean,

    local: Record<LuaID, PyID>,
    not_local__py_local: Record<LuaID, PyID>,
    nonlocal: Record<LuaID, PyID>,
    global: Record<LuaID, PyID>,

    is_py_global: boolean,

    py_local: Record<PyID, true>,
    py_nonlocal: Record<PyID, true>,
    py_global: Record<PyID, true>,

    used_py_nonlocal: Record<PyID, true>,
    used_py_global: Record<PyID, true>,
  }
  ) {
    Object.assign(this, args)
    this._gensym_c = 0
  }
}

function null_or_undefined_p(x): x is null | undefined | void {
  return x == null
}

function not_null_or_undefined_p(x): boolean {
  return x != null
}

function object_copy<T extends object>(o:T):T{
  return Object.assign(Object.create(o),o)
}
function record_copy<K extends string | number | symbol,V>(o:Record<K,V>):Record<K,V> {
  return Object.assign({}, o)
}
function record_merge<K extends string | number | symbol,V>(o1:Record<K,V>, o2:Record<K,V>):Record<K,V> {
  return Object.assign(record_copy(o1), o2)
}

function scope_add_local(scope: Scope, id: LuaID): void {
  assert_false(id in scope.local, () => `Redefine ${id}.`)
  let c = 0

  function g(): string {
    if (c === 0) {
      return id
    } else {
      return id + "____" + c.toString()
    }
  }
  function has(x: string): boolean {
    return x in scope.py_local || x in scope.py_nonlocal || x in scope.py_global
  }
  while (has(g())) {
    c++
  }
  const ret = g()
  scope.local[id] = ret
  scope.py_local[ret] = true
}

function id_lua2py(id: LuaID, scope: Scope): PyID {
  if (id in scope.local) {
    return scope.local[id]
  } else if (id in scope.not_local__py_local) {
    return scope.not_local__py_local[id]
  } else if (id in scope.nonlocal) {
    const ret = scope.nonlocal[id]
    scope.used_py_nonlocal[ret] = true
    return ret
  } else if (id in scope.global) {
    const ret = scope.global[id]
    scope.used_py_global[ret] = true
    return ret
  }
  return assert_fail(`${id} is not defined.`)
}


const init_stats: Parameter<Array<string>> = make_parameter([])
const table: Record<string, (x: LuaAST, s: Scope) => string | void> = {
  NumericLiteral: (x, scope) => x.raw,
  Chunk: (x, scope) => {
    assert_true(scope.is_py_global && scope.is_global)
    init_stats().push(...(x.body.map((v) => compile(v, scope)).flat().filter(not_null_or_undefined_p)))
  },
  DoStatement: (x, outter_scope) => {
    const inner_scope = object_copy(outter_scope)
    inner_scope.local = {}
    inner_scope.is_global = false
    inner_scope.not_local__py_local = record_merge(outter_scope.not_local__py_local, outter_scope.local)
    init_stats().push(...(x.body.map((v) => compile(v, inner_scope)).flat().filter(not_null_or_undefined_p)))
  },
  AssignmentStatement: compile_LocalStatement_AssignmentStatement,
  LocalStatement: compile_LocalStatement_AssignmentStatement,
  FunctionDeclaration: compileFunctionDeclaration,
}

function compile_LocalStatement_AssignmentStatement(x: LuaAST, scope: Scope): void {
  const {
    variables,
    init: inits
  } = x
  assert_true(variables.length !== 0)
  if (inits.length === 0) {
    for (const variable of variables) {
      assert_equal(variable.type, 'Identifier')
      if (x.type === 'LocalStatement') {
        scope_add_local(scope, variable.name)
      } else {
        assert_equal(x.type, 'AssignmentStatement')
      }
      init_stats().push(`${id_lua2py(variable.name, scope)}=None`)
    }
  }
  assert_equal(variables.length, 1, () => `Unsupported multiple assignment at ${print_loc_of(x)}`)
  assert_equal(inits.length, 1, () => `Unsupported multiple assignment at ${print_loc_of(x)}`)
  const [variable] = variables
  assert_equal(variable.type, 'Identifier')
  const [init] = inits
  if (x.type === 'LocalStatement') {
    scope_add_local(scope, variable.name)
  } else {
    assert_equal(x.type, 'AssignmentStatement')
  }
  init_stats().push(`${id_lua2py(variable.name, scope)}=${compile_expression(init, scope)}`)
}

function compile(x: LuaAST, scope: Scope): string | void {
  assert_true(x.type in table, () => `Unsupported node "${x.type}" at ${print_loc_of(x)}`)
  return table[x.type](x, scope)
}

export { compile }

function compile_expression(x: LuaAST, scope: Scope): string {
  const ret = compile(x, scope)
  if (null_or_undefined_p(ret)) {
    return assert_fail()
  } else {
    return ret
  }
}

function compileFunctionDeclaration(x, outter_scope) {
  assert_equal(x.type, 'FunctionDeclaration')
  const {
    identifier,
    isLocal,
    parameters,
    body
  } = x

  function genargs() {
    throw 'WIP'
  }
  if (identifier !== null) {
    if (isLocal || outter_scope.is_global) {
      assert_equal(identifier.type, 'Identifier')
      scope_add_local(outter_scope, identifier.name)
      init_stats().push(`def ${id_lua2py(identifier.name, outter_scope)}(${genargs()}):`)
      parameterize(init_stats, [], () => {
        const inner_scope='WIP'
      })
      throw 'WIP'
    } else {
      const f2 = object_copy(x)
      f2.identifier = null
      return compile({
        type: 'AssignmentStatement',
        variables: [identifier],
        init: [f2],
        loc: x.loc
      }, outter_scope)
    }
  } else {
    assert_true(identifier === null)
    throw 'WIP'
  }
  return assert_fail()
}
