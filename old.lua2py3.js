/*
    The Language
    Copyright (C) 2019  Zaoqi <zaomir@outlook.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

// [Polyfill] https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flat
if (!Array.prototype.flat) {
  Array.prototype.flat = function(depth) {
    var flattend = [];
    (function flat(array, depth) {
      for (let el of array) {
        if (Array.isArray(el) && depth > 0) {
          flat(el, depth - 1);
        } else {
          flattend.push(el);
        }
      }
    })(this, Math.floor(depth) || 1);
    return flattend;
  };
}

const lua_parse = (() => {
  const p = require('luaparse')

  function luaparse(x) {
    const ast = p.parse(x, {
      comments: false,
      scope: false,
      locations: true
    })
    return ast
  }
  return luaparse
})()

const assert_strict = require('assert').strict

function assert_equal(x, y, msg = () => null) { // msg懶惰求值。
  assert_strict.equal(x, y, {
    toString() {
      return msg();
    }
  })
}

function assert_notEqual(x, y, msg = () => null) {
  assert_strict.notEqual(x, y, {
    toString() {
      return msg();
    }
  })
}

function assert_fail(msg = null) {
  assert_strict.ok(false, msg)
}

function assert_true(x, msg = () => null) {
  assert_strict.ok(x, {
    toString() {
      return msg();
    }
  })
}

function assert_false(x, msg = () => null) {
  return assert_true(!x, msg)
}

// like https://docs.racket-lang.org/reference/parameters.html
function make_parameter(v = undefined) {
  let context = v
  const nothing = []

  function parameter(arg = nothing) {
    if (arg === nothing) {
      return context
    } else {
      context = arg
    }
  }
  return parameter
}

function parameterize(v, x, body) {
  const before = v()
  v(x)
  const ret = body()
  v(before)
  return ret
}


function null_or_undefined_p(x) {
  return x == null
}

function not_null_or_undefined_p(x) {
  return x != null
}

function object_copy(o) {
  return Object.assign({}, o)
}

function object_merge(o1, o2) {
  return Object.assign(object_copy(o1), o2)
}

function array_copy(xs) {
  return xs.slice()
}

function print_loc_of(x) {
  const {
    loc: {
      start: {
        line: loc_start_line,
        column: loc_start_column
      },
      end: {
        line: loc_end_line,
        column: loc_end_column
      }
    }
  } = x
  return `((start (line ${loc_start_line}) (column ${loc_start_column})) (end (line ${loc_end_line}) (column ${loc_end_column})))`
}

function py_indent(code) {
  return code.map((x) => '    ' + x)
}

function id_lua2py(id, scope) {
  if (id in scope.local) {
    return scope.local[id]
  } else if (id in scope.not_local__py_local) {
    return scope.not_local__py_local[id]
  } else if (id in scope.nonlocal) {
    const ret = scope.nonlocal[id]
    scope.used_py_nonlocal[ret] = true
    return ret
  } else if (id in scope.global) {
    const ret = scope.global[id]
    scope.used_py_global[ret] = true
    return ret
  }
  return assert_fail(`${id} is not defined.`)
}

function scope_add_local(scope, id) {
  assert_false(id in scope.local, () => `Redefine ${id}.`)
  let c = 0

  function g() {
    if (c === 0) {
      return id
    } else {
      return id + "____" + c.toString()
    }
  }
  while (g() in scope.py_local) {
    c++
  }
  const ret = g()
  scope.local[id] = ret
  scope.py_local[ret] = true
}
const init_stats = make_parameter()
const table = {
  Chunk: (x, scope) => {
    assert_true(scope.is_py_global && scope.is_global)
    init_stats().push(...(x.body.map((v) => compile(v, scope)).flat().filter(not_null_or_undefined_p)))
  },
  DoStatement: (x, scope) => {
    const inner_scope = object_copy(scope)
    inner_scope.local = {}
    inner_scope.is_global = false
    inner_scope.not_local__py_local = object_merge(scope.not_local__py_local, scope.local)
    init_stats().push(...(x.body.map((v) => compile(v, inner_scope)).flat().filter(not_null_or_undefined_p)))
  },
  AssignmentStatement: compile_LocalStatement_AssignmentStatement,
  LocalStatement: compile_LocalStatement_AssignmentStatement,
  NumericLiteral: (x, scope) => x.raw,
  FunctionDeclaration: compileFunctionDeclaration,
}

function compileFunctionDeclaration(x, scope) {
  assert_equal(x.type, 'FunctionDeclaration')
  const {
    identifier,
    isLocal,
    parameters,
    body
  } = x

  function genargs() {
    throw 'WIP'
  }
  if (identifier !== null && (isLocal || scope.is_global)) {
    assert_equal(identifier.type, 'Identifier')
    scope_add_local(scope, identifier.name)
    init_stats().push(`def ${id_lua2py(identifier.name,scope)}(${genargs()}):`)
    parameterize(init_stats, [], () => {
      const inner_scope = {
        is_py_global: scope.is_py_global,
        is_global: false,

        local: {}, // Map<LuaId, PyId>
        not_local__py_local: object_merge(scope.local, scope.not_local__py_local), // Map<LuaId, PyId>
        nonlocal: WIP, // Map<LuaId, PyId>
        global: WIP, // Map<LuaId, PyId>

        py_local: WIP, // Map<PyId, True>

        used_py_global: WIP, // Map<PyId, True>
        used_py_nonlocal: WIP, // Map<PyId, True>

        gensym: WIP
      }
    })
    throw 'WIP'
  } else if (identifier !== null) {
    const f2 = object_copy(x)
    f2.identifier = null
    return compile({
      type: 'AssignmentStatement',
      variables: [identifier],
      init: [f2],
      loc: x.loc
    }, scope)
  } else {
    assert_true(identifier === null)
    throw 'WIP'
  }
  return assert_fail()
}

function compile_LocalStatement_AssignmentStatement(x, scope) {
  const {
    variables,
    init: inits
  } = x
  assert_true(variables.length !== 0)
  if (inits.length === 0) {
    for (const variable of variables) {
      assert_equal(variable.type, 'Identifier')
      if (x.type === 'LocalStatement') {
        scope_add_local(scope, variable.name)
      } else {
        assert_equal(x.type, 'AssignmentStatement')
      }
      init_stats().push(`${id_lua2py(variable.name,scope)}=None`)
    }
  }
  assert_equal(variables.length, 1, () => `Unsupported multiple assignment at ${print_loc_of(x)}`)
  assert_equal(inits.length, 1, () => `Unsupported multiple assignment at ${print_loc_of(x)}`)
  const [variable] = variables
  assert_equal(variable.type, 'Identifier')
  const [init] = inits
  if (x.type === 'LocalStatement') {
    scope_add_local(scope, variable.name)
  } else {
    assert_equal(x.type, 'AssignmentStatement')
  }
  init_stats().push(`${id_lua2py(variable.name,scope)}=${compile_expression(init,scope)}`)
}

function compile(x, scope) {
  assert_true(x.type in table, () => `Unsupported node "${x.type}" at ${print_loc_of(x)}`)
  return table[x.type](x, scope)
}

function compile_expression(x, scope) {
  const ret = compile(x, scope)
  assert_true(not_null_or_undefined_p(ret))
  return ret
}

function global_compile(x) {
  return parameterize(init_stats, [], () => {
    const global = {}
    const gensym_count = 0
    const a_undefined = compile(lua_parse(x), {
      is_py_global: true,
      is_global: true,

      local: global, // Map<LuaId, PyId>
      not_local__py_local: {}, // Map<LuaId, PyId>
      nonlocal: {}, // Map<LuaId, PyId>
      global: global, // Map<LuaId, PyId>

      py_local: {}, // Map<PyId, True>

      used_py_global: {}, // Map<PyId, True>
      used_py_nonlocal: {}, // Map<PyId, True>

      gensym: () => {
        gensym_count += 1
        return "tmp____" + gensym_count.toString()
      }
    })
    assert_equal(a_undefined, undefined)
    let ret = ""
    const xs = init_stats()
    for (const x of xs) {
      ret += x + "\n"
    }
    return ret
  })
}

module.exports = {
  lua_parse: lua_parse,
  global_compile: global_compile,
}